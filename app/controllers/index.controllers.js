// Récupération du modèle de postgressql
const Joi = require('joi');
const { Pool} = require('pg');
// Paramétres d'accès à la base de données
const pool = new Pool ({
    host: 'localhost',
    user: 'postgres',
    password: '123',
    database: 'gestcompagepublicitaire',
    port: '5432'
});

// Lister les annonceurs
const getAnnonceurs = async (req, res) => {
    try{
        const response = await 
        pool.query('SELECT * FROM annonceur ORDER BY id ASC');
        if (response.rowCount==0)
            res.status(200).json('Aucun Annonceur exist!!');
        else 
            res.status(200).json(response.rows);
    } catch (err) {
        console.log(err);
    }
};

// Récupération d'un annonceur par son Id
const getAnnonceurById = async (req, res) => {
    try{
        const id = req.params.id;
        const response = await 
        pool.query(`SELECT * FROM annonceur WHERE id = ${id}`);
        if (response.rowCount==0)
            res.status(200).json(`Annonceur numéro ${id} non exist`);
        else 
            res.status(200).json(response.rows);
    } catch (err) {
        console.log(err);
    }
};

// Ajout d'un nouveau annonceur
const createAnnonceur = async (req, res) => {
    try {
        //Validation  des entrées
        const { error } = validateAnnonceur(req.body);
        if(error)
            return res.status(400).send(error.details[0].message);
        const {libelle, email, tel} = req.body;
        const requete = 'INSERT INTO annonceur '+
        '(libelle, email, tel)'
        +' VALUES ($1, $2, $3)';
        const response = await pool.query(requete,[libelle, email, tel]);
        console.log(response);
        res.json({
           message:  'Annonceur Ajouté avec succès!',
           body: {
               annonceur: {libelle, email, tel}
           }
        })
    } catch (error) {
        console.log(error);
    }
};

// Eléminer un annonceur par son Id 
const deleteAnnonceur = async (req, res) => {
    try {
        const id = req.params.id;
        const response = await pool.query(`DELETE FROM annonceur WHERE id = ${id}`);
        if(response.rowCount == 1)
            res.status(200).json(`Annonceur numéro ${id} est bien supprimé!!`);
        else
            res.status(200).json(`Annonceur numéro ${id} non exist!!!`);
    } catch (error) {
        console.log(error);
    }
};

// Supprimer tous les annonceur
const deleteAnnonceurs = async (req, res) => {
    try {
        const response = await pool.query('DELETE FROM annonceur');
        if(response.rowCount == 0)
            res.status(200).json(`Aucuns annonceur éxistant !!!`);    
        else
            res.status(200).json(`Tout les annonceurs ont bien été supprimés !!`);    
    } catch (error) {
        console.log(error);
    }
};

// Modification d'un annonceur par son Id 
const updateAnnonceur = async (req, res) => {
    try {
        //Validation  des entrées
        const { error } = validateAnnonceur(req.body);
        if(error)
            return res.status(400).send(error.details[0].message);
        const id = req.params.id;
        const {libelle, email, tel} = req.body;
        const requete = `UPDATE annonceur SET libelle = $1, email = $2, `+
        `tel = $3 WHERE id = ${id}`;
        const response = await pool.query(requete,[libelle, email, tel]);
        if(response.rowCount == 0 )
            res.status(200).json(`Annonceur numéro ${id} n'exist pas !!!`);
        else{
            res.json({
                message:  `Annonceur numéro ${id} est modifié avec succès!`,
                body: {
                    annonceur: {libelle, email, tel}
                }
            })
        }
    } catch (error) {
        console.log(error);
    }
};
////////////////Partie Compagne///////////////////////




// Utilisation  de la classe Joi pour valider un salarié
function validateAnnonceur(annonceur) {
    const schema = {
        libelle: Joi.string().min(3).required(),
        email: Joi.string().min(3).email().required(),
        tel: Joi.number().min(10).required()
    };
    return Joi.validate(annonceur, schema);
}


// Exportations des actions
module.exports = {
    getAnnonceurs, getAnnonceurById, createAnnonceur,
    deleteAnnonceur, deleteAnnonceurs, updateAnnonceur
    

}