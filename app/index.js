const express = require('express');
const app = express();

// middlewares
app.use(express.json())
.use(express.urlencoded({extended: false}));

// routes
app.use(require('./routes/index'));

// PORT
const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Serveur écoute on port ${port}...`));