const { Router } = require('express');
const router = Router();

const { getAnnonceurs, getAnnonceurById, createAnnonceur,
    deleteAnnonceur, deleteAnnonceurs, updateAnnonceur
      }
     = require('../controllers/index.controllers');

// Manipulation des routes sannonceurs
router.get('/gestionpublicites/annonceurs', getAnnonceurs)
.post('/gestionpublicites/annonceurs', createAnnonceur)
.get('/gestionpublicites/annonceurs/:id', getAnnonceurById)
.delete('/gestionpublicites/annonceurs/:id',deleteAnnonceur)
.delete('/gestionpublicites/annonceurs/', deleteAnnonceurs)
.put('/gestionpublicites/annonceurs/:id',updateAnnonceur);


// Manipulation des routes compagnes publicitaires


module.exports = router;