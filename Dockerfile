FROM ubuntu:latest
MAINTAINER Redouane
RUN apt-get update -yq \
&& apt-get install curl gnupg -yq \
&& curl -sL https://deb.nodesource.com/setup_12.x | bash \
&& apt-get install nodejs -yq \
&& apt-get clean -y

ADD . /app/
WORKDIR /app
RUN npm install express \
&& npm install pg \
&& npm install -D nodemon@2.0.2 \
&& npm install -D joi@14.3.1

EXPOSE 2368
VOLUME /app/logs

CMD npm run start
